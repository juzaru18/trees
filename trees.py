# Author : Juan Zamorano Ruiz
# Copyright: MIT License
# e-mail: juzaru18@gmail.com


# General import for python and sklearn
import pandas as pd
import seaborn as sns
import numpy as np
import time as t
import matplotlib.pyplot as plt
import itertools

import os
import warnings
import gc


from memory_profiler import profile

from matplotlib.colors import ListedColormap
from time import time
from sklearn.model_selection import StratifiedShuffleSplit, train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler

from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score, log_loss, recall_score, precision_score, confusion_matrix, classification_report
from sklearn.model_selection import GridSearchCV

# Import some of classifiers to test
from sklearn.neighbors import KNeighborsClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression, Perceptron
from sklearn.svm import SVC, LinearSVC, NuSVC, SVR
from sklearn import linear_model
from sklearn.semi_supervised import LabelPropagation
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, BaggingClassifier, \
    ExtraTreesClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.externals.six import StringIO  



def preprocess(train):
    """
    Given train dataset from Forest Cover Type dataset, it returns
    two dataframes, one with just labels and one with train dataset with all
    atritubes except CoverType atribute
    """

    labels = train.Cover_Type.values

    train = train.drop(['Cover_Type'], axis=1)

    return train, labels


def standardize (train):
    """
    given a forest cover type dataset, this function transform the data
    to StandardScaler [-1,1] scale
    Returns just first 10 atributes on tha scale concatenate with the other
    atributes
    """

    train1 = train

    Size = 10

    X_temp1 = train1.iloc[:, :Size]

    # Transform to StandardScaler
    X_temp1 = StandardScaler().fit_transform(X_temp1)

    r, c = train.shape
    X_train1 = np.concatenate((X_temp1, train1.iloc[:, Size:c - 1]), axis=1)  

    return X_train1

def minmaxscalizer (train):
    """
    given a forest cover type dataset, this function transform the data
    to MinMaxScaler [0,1] scale
    Returns just first 10 atributes on tha scale concatenate with the other
    atributes
    """

    train1 = train

    Size = 10

    X_temp1 = train1.iloc[:, :Size]

    # Transform to MinMaxScaler
    X_temp1 = MinMaxScaler().fit_transform(X_temp1)

    r, c = train.shape
    X_train1 = np.concatenate((X_temp1, train1.iloc[:, Size:c - 1]), axis=1)  

    return X_train1

##############################################################################################################
####################### Grid Search##########################################################################
#Reference: 
#http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV


# Utility function to report best scores
def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0} ".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(results['mean_test_score'][candidate],
                                                                         results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")


# Use a full grid over all parameters. It requires more computation


def grid_search(classifier, parameters):
    """
    GridSearch for a classifier given a dict of parameters to check
    Returns a tuple with the best parameters to use to get the best accuracy score
    """
    clf = classifier
    # param = parameters
    print("=" * 100)
    print("GridSearch with classifier %s with parameters %s" % (clf.__class__.__name__, parameters))
    grid = GridSearchCV(clf, param_grid=parameters, scoring='accuracy', cv=2, n_jobs=-1, verbose=2)
    start = time()
    grid.fit(X_train, y_train)

    print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
          % (time() - start, len(grid.cv_results_['params'])))
    print("The best parameters for classifier %s  is %s with a score of %0.2f" % (clf.__class__.__name__,
                                                                                  grid.best_params_,
                                                                                  grid.best_score_))

    print("=" * 100)


############# Range of variables for gridsearch functions ###################

loss_range = ['hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron']
penalty_range = ['none', 'l2', 'l1', 'elasticnet']
criterion_range = ['entropy', 'gini']
alpha_range = [0.001, 0.01, 0.1]
max_iter_range = [1000, 2000]
random_state_range = [1, 5, 10]
tol_range = [0.01, 0.1]
C_range = np.logspace(-2, 10, 8)
n_neighbors_range = [1, 2, 5, 10]
max_samples_range = [1, 10, 1000, 5000, 6000]
max_features_range = [10, 20, 30, 40, 50]
max_depth_range = [2, 4, 6, None]
min_samples_split_range = [2, 5, 7, 10]
min_samples_leaf_range = [1, 3, 6, 8, 10]
min_weight_fraction_leaf_range = [0.0, 0.1, 0.5]
###################################################################################

""" List of dicts for grid search for each classifier """

DecisionTreeGrid = dict(max_depth=[2, 4, 6, None],
                        max_features=[10, 20, 30, 40, 50, None],
                        min_samples_split=[2, 5, 7, 10],
                        min_samples_leaf=[1, 3, 6, 8, 10],
                        min_weight_fraction_leaf=[0.0, 0.1, 0.5],
                        splitter=["best", "random"],
                        criterion=["gini", "entropy"],
                        random_state=[1, 5, 10]),

RandomForestGrid = dict(max_depth=[3, None], max_features=[1, 3], min_samples_split=[2, 3, 10],
                        min_samples_leaf=[1, 3, 10],
                        bootstrap=[True, False], criterion=["gini", "entropy"])

AdaBoostGrid = dict(base_estimator = [RandomForestClassifier(), 
                                      DecisionTreeClassifier(),
                                      ExtraTreesClassifier()],
                    learning_rate=[1, 3, 5, 7],
                    algorithm=['SAMME', 'SAMME.R'], 
                    random_state=[None, 3, 7, 10],
                    n_estimators = [30, 40, 50, 70])

GradientBoostingGrid = dict(loss=['deviance'],
                            learning_rate=[0.1, 0.5, 1],
                            verbose=[1],
                            max_depth=[2, None],
                            criterion=["friedman_mse", "mse", "mae"],
                            min_samples_split=[2],
                            min_samples_leaf=[1],
                            min_weight_fraction_leaf=[0.5],
                            max_features=[1],
                            max_leaf_nodes=[7],
                            subsample=[1.0],
                            warm_start=[True],
                            random_state=[10])

LogisticRegressionGrid = dict(fit_intercept=[True, False],
                              random_state=[1, 2],
                              solver=['newton-cg', 'lbfgs', 'sag', 'saga'],
                              max_iter=[20, 30, 100, 200],
                              multi_class=['multinomial', 'ovr'],
                              warm_start=[True, False])
                              # 4 hours for GridSearch

SGDClassifierGrid = dict(loss=loss_range,
                         penalty=['none', 'l2', 'l1', 'elasticnet'],
                         learning_rate=[0.1, 0.5, 1],
                         alpha=[0.001, 0.01, 0.1],
                         l1_ratio=[0.01, 0.30],
                         fit_intercept=[False, True],
                         max_iter=[100, 200])

PerceptronGrid = dict(penalty=penalty_range,
                      alpha=alpha_range,
                      fit_intercept=[True, False],
                      shuffle=[True, False],
                      warm_start=[True, False],
                      random_state=random_state_range)
                      # 9 minutes for GridSearch 

PassiveAgressiveGrid = dict(C=C_range,
                            fit_intercept=[True, False],
                            tol=tol_range,
                            shuffle=[True, False],
                            loss=loss_range,
                            max_iter=max_iter_range,
                            random_state=random_state_range,
                            n_iter=max_iter_range,
                            warm_start=[True, False],
                            average=[True, False])

KneighborsGrid = dict(n_neighbors=n_neighbors_range,
                      #weights=['uniform', 'distance'],
                      #algorithm=['auto', 'ball_tree', 'kd_tree', 'brute'],
                      p=[1, 2])

BaggingClassifierGrid = dict(max_samples=max_samples_range,
                             max_features=max_features_range,
                             bootstrap=[True, False],
                             bootstrap_features=[True, False],
                             random_state=random_state_range)

ExtraTreesGrid = dict(criterion=criterion_range,
                      max_features=[9, 10, "auto", "sqrt", "log2"],
                      max_depth=max_depth_range,
                      min_samples_split=min_samples_split_range,
                      min_samples_leaf=min_samples_leaf_range,
                      min_weight_fraction_leaf=min_weight_fraction_leaf_range,
                      max_leaf_nodes=[None, 3, 7],
                      bootstrap=[True],
                      oob_score=[True, False],
                      n_jobs=[1, -1],
                      random_state=random_state_range,
                      warm_start=[True, False])

MLPGrid = dict(hidden_layer_sizes=(4, 5, 7),
               activation=['identity', 'logistic', 'tanh', "relu"],
               solver=['lbfgs', 'sgd', 'adam'],
               alpha=alpha_range,
               batch_size=[100, 200],
               learning_rate=['constant', 'invscaling', 'adaptive'])
               #10 hours for GridSearch

SVCGrid = dict(degree = [2,3,4],
               shrinking = [True, False],
               tol = tol_range,
               decision_function_shape = ['ovr', 'ovo'],
               random_state = random_state_range,
               C = [0.1, 1, 10, 100]
               )

LinearDiscriminantGrid = dict(solver=["svd","lsqr","eigen"],
                              store_covariance=[True,False],
                              tol=tol_range)
                              #28 seconds
QuadraticDiscriminantGrid = dict(store_covariance = [True,False],
                                 tol = tol_range)
                              #25 seconds
                              

#########################################################################################
# Reference:
# http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
def plot_confusion_matrix(cm,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    classes = ['Spruce/Fir', 'Lodgepole Pine', 'Ponderosa Pine', 
               'Cottonwood/Willow', 'Aspen', 'Douglas-fir', 'Krummholz',]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("matriz de confusion normalizada")
    else:
        print('matriz de confusion sin normalizar')

    print(cm)
    plt.imshow(cm, interpolation='nearest', cmap=cmap)

    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

###########################################################################################################
# @profile is used to extract memory used on fit for each classifier
@profile(precision=2)
def fitting(classifier, x, y):
    classifier.fit(x, y)

def compare_classifiers(classifiers,
                        X_train,
                        y_train,
                        X_test,
                        y_test,
                        conf_matrix=False):
    """Given a list o classifiers and datasets, it returns a dataframe with values of fit for each classifier, if
    conf_matrix is set to True then it will plot a figure with a confusion matrix for each classifier in the list"""

    log_cols = ["Classifier", "Accuracy", "Score Train", "Log Loss", "Precision Score", 
                "Recall Score", "Time to fit", "Time to predict"]
    log = pd.DataFrame(columns=log_cols)
    
    for clf in classifiers:

        print("=" * 30)
        print("Fit for classifier --> ", clf.__class__.__name__)

        start = time()

        fitting(clf, X_train, y_train)
        # clf.fit(X_train, y_train)


        time_spent = (time() - start)
        score = clf.score(X_test, y_test)
        print(" took %.2f seconds" % (time() - start))

        print("=" * 30)
        print(clf.__class__.__name__)

        print("***** Results *****")
        time_init_pred = time()
        y_pred = clf.predict(X_test)
        time_predict = time() - time_init_pred
        print(" took %.2f seconds to predict on test dataset" % time_predict)
        
        acc = accuracy_score(y_test, y_pred)

        ############# Precision #########################
        # The precision is the ratio tp / (tp + fp) where tp is the number of true positives and fp the number of false positives
        # The precision is intuitively the ability of the classifier not to label as positive a sample that is negative
        precision = precision_score(y_test, y_pred, average='weighted')

        ############# Recall Score ######################
        # The recall is the ratio tp / (tp + fn) where tp is the number of true positives and fn the number of false negatives
        # The recall is intuitively the ability of the classifier to find all the positive samples
        recall = recall_score(y_test, y_pred, average='weighted')

        print("Accuracy of the classifier: {:.3%}".format(acc))
        print("Precision score of the classifier: {:.3%}".format(precision))
        print("Score of the classifier: {:.3%}.".format(score))
        
        class_report = classification_report(y_test, y_pred)
        print ("Classification report: ")
        print(class_report)
        if hasattr(clf, "predict_proba"):
            train_predictions = clf.predict_proba(X_test)
        else:
            conf_mat = confusion_matrix(y_true=y_test, y_pred=y_pred)
            train_predictions = clf.decision_function(X_test)
        
        #plot confusion matrix if variable conf_matrix is set to True
        if conf_matrix:
            cnf_matrix = confusion_matrix(y_test, y_pred)
            np.set_printoptions(precision=2)

            # Plot non-normalized confusion matrix
            plt.figure()
            plot_confusion_matrix(cnf_matrix, title='matriz de confusion sin normalizacion')

            # Plot normalized confusion matrix
            plt.figure()
            plot_confusion_matrix(cnf_matrix, normalize=True,
                                  title='matriz de confusion con normalizacion')
            plt.show()


        ll = log_loss(y_test, train_predictions)
        print("Compute recall of the classifier: {:.3%}".format(recall))
        print("Log Loss: {}".format(ll))
        entry = pd.DataFrame([[clf.__class__.__name__, acc * 100, score * 100,
                               ll, precision * 100, recall*100, time_spent, time_predict]], columns=log_cols)
        log = log.append(entry)
        print("=" * 30)
    return log



#########################################Show graph values #############################################################

def show_graphs(table):
    """
    Given a panda dataframe with different values, show different graphs for comparasion of different classifiers
    accuracy, log loss, Time spent, Precision Score, Recall Score, time to predict
    :param table: a panda dataframe with some values of some classifiers
    :return: some seaborn plots with graphical info
    Uncomment to show the plots
    """
    values_sorted = table.sort_values(["Exactitud(%)", "Sensibilidad"], ascending=False)
    values_top10 = values_sorted.head(10)
    values_bottom10 = values_sorted.tail(10)
    time_sorted = table.sort_values(["Tiempo de ajuste (tiempo en segundos)"])
    time_top10 = time_sorted.head(10)
    time_bottom10 = time_sorted.tail(10)
    log_sorted = table.sort_values(["Log Loss"])
    log_top10 = log_sorted.head(10)
    log_bottom10= log_sorted.tail(10)
    precision_sorted = table.sort_values(["Precision Score"], ascending=False)
    time_predict_sorted = table.sort_values(["Tiempo de prediccion"])
    memory_sorted = table.sort_values(["Memoria"])
    
    
    

    sns.set(style="dark", context="paper")
    sns.set()

    sns.set_palette("husl",8)

    #sns.barplot(x='Exactitud(%)', y='Clasificador', data=values_sorted, label="Exactitud")
    #plt.title("Mejor cuanto mas cerca de 100")
    #plt.show()

    #sns.barplot(x='Sensibilidad', y='Clasificador', data=values_sorted, label="Sensibilidad")
    #plt.title("Mejor cuanto mas cerca de 100, %")
    #plt.show()

    #sns.barplot(x='Log Loss', y='Clasificador', data=log_sorted, label="Log Loss")
    #plt.title("Mejor cuanto mas cercano a 0")
    #plt.show()

    #sns.barplot(x='Precision Score', y='Classifier', data=precision_sorted, label="Precision Score")
    #plt.title("Mejor cuanto mas cerca de 100, %")
    #plt.show()

    #sns.barplot(x='Tiempo de ajuste (tiempo en segundos)', y='Clasificador', data=time_sorted, label="Tiempo de ajuste")
    #plt.title("Mejor cuanto menor")
    #plt.show()
    
    #sns.barplot(x='Tiempo de prediccion', y='Classifier', data=time_predict_sorted, label="Tiempo de predicci�n")
    #plt.title("Mejor cuanto menor, tiempo en segudndos")
    #plt.show()
    
    #sns.barplot(x='Memoria', y='Classifier', data=memory_sorted, label="Memoria usada")
    #plt.title("Mejor cuanto menor, memoria en MB")
    #plt.show()
    
    
    sns.despine(bottom=True)

###############################################################################

def plot_decision_regions(X, y, classifier, test_idx=None, resolution=0.02):
    markers = ('s', 'x', 'o', '^', 'v', 'p', 'd')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan', 'yellow', 'black')
    cmap = ListedColormap(colors[:len(np.unique(y))])

    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                           np.arange(x2_min, x2_max, resolution))
    z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    z = z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, z, alpha=0.4, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())

    # plot all samples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0], y=X[y == cl, 1],
                    alpha=0.8, c=cmap(idx),
                    marker=markers[idx], label=cl)
    # highlight test samples
    if test_idx:
        X_test, y_test = X[test_idx, :], y[test_idx]
        plt.scatter(X_test[:, 0], X_test[:, 1], c='',
                    alpha=1.0, linewidths=1, marker='o',
                    s=55, label='test set')


#################### Plotting Comparasion of classifiers
# Reference:
#http://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html

def plot_compare_classifiers(X_set, y_set, classif):
    """
    Given a X_set dataset, y_set associated to labels of first dataset and a list of estimators
    :param X_set: dataset with a number of features
    :param y_set: labels for each dataset
    :param classif: list of estimators.
    :return: taken 50 points of X_set and y_set, plot decision boundaries of different classifiers.
    """
    h = .02  # step size in the mesh

    """names = ["Logistic", "SGD", "Linear SVC", "SVC RBF", "Neighbors", "Decision Tree",
             "AdaBoost", "Gradient Boosting", "NB", "ExtraTree", "MLP", "QDA", "Linear", "Gaussian"]"""
    names_linear = ["Logistic", "SGD", "PassiveAgressive", "Perceptron", "Ridge"]

    names_discriminant = ["Linear Discriminant", "Quadratic Discriminant"]

    names_support_vector = ["SVC", "NuSVC"]

    names_neighbors_gaussian_neural = ["Kneighbors", "Gaussian Process", "MultiLayer Perceptron"]

    names_naive_bayes = ["Bernouilli NB", "Gaussian NB", "Multinomial NB"]

    names_trees = ["Decision Tree", "Extra Tree"]

    names_ensemble = ["AdaBoost Extra Trees", "AdaBoost Random Forests", "Perceptron"]

    names_network = [" Multilayer Perceptron"]


    X = X_set.loc[:50, 'Elevation':'Aspect']
    y = y_set[:51]
    rng = np.random.RandomState(2)
    X += 2 * rng.uniform(size=X.shape)
    linearly_separable = (X, y)

    datasets = [linearly_separable]

    figure = plt.figure(figsize=(27, 9))
    i = 1
    # iterate over datasets

    for ds_cnt, ds in enumerate(datasets):
        # preprocess dataset, split into training and test part
        X, y = ds
        """Atention, for Naive Bayes use MinMaxScaler to avoid negative values on X dataset"""
        # X = MinMaxScaler().fit_transform(X)
        X = StandardScaler().fit_transform(X)

        X_train, X_test, y_train, y_test = \
            train_test_split(X, y, test_size=.4, random_state=42)

        x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5

        y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))

        # just plot the dataset first
        cm = plt.cm.RdBu
        cm_bright = ListedColormap(['#FF0000', '#0000FF'])
        ax = plt.subplot(len(datasets), len(classif) + 1, i)
        if ds_cnt == 0:
            ax.set_title("Input data")
        # Plot the training points
        ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright,
                   edgecolors='k')
        # and testing points
        ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright, alpha=0.6,
                   edgecolors='k')
        ax.set_xlim(xx.min(), xx.max())
        ax.set_ylim(yy.min(), yy.max())
        ax.set_xticks(())
        ax.set_yticks(())
        i += 1

        # iterate over classifiers
        for name, clf in zip(names_ensemble, classif):
            ax = plt.subplot(len(datasets), len(classif) + 1, i)
            clf.fit(X_train, y_train)
            score = clf.score(X_test, y_test)

            # Plot the decision boundary. For that, we will assign a color to each
            # point in the mesh [x_min, x_max]x[y_min, y_max].
            if hasattr(clf, "decision_function"):
                z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
            else:
                z = clf.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]
            # Put the result into a color plot
            z = z.reshape(xx.shape)
            ax.contourf(xx, yy, z, cmap=cm, alpha=.8)

            # Plot also the training points
            ax.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap=cm_bright,
                       edgecolors='k')
            # and testing points
            ax.scatter(X_test[:, 0], X_test[:, 1], c=y_test, cmap=cm_bright,
                       edgecolors='k', alpha=0.6)

            ax.set_xlim(xx.min(), xx.max())
            ax.set_ylim(yy.min(), yy.max())
            ax.set_xticks(())
            ax.set_yticks(())
            if ds_cnt == 0:
                ax.set_title(name)
            ax.text(xx.max() - .3, yy.min() + .3, ('%.2f' % score).lstrip('0'),
                    size=15, horizontalalignment='right')
            i += 1

    plt.tight_layout()
    plt.show(figure)


########################################################################################
def pca(classifiers, x_train, y_train, x_test, y_test, n, conf_matrix=False):
    """
    Given a
    :param classifiers: list of classifiers to do PCA algorithm
    :param x_train: x train datatset for training
    :param y_train: y train labels for training dataset
    :param x_test: x test dataset to classify
    :param y_test: y test dataset
    :param n: number of components to keep for PCA
    :return: a pandas dataframe with values of accuracy, Score, Log Loss, Precision Score, Recall Score and Time spent for each
            classifier
    """
    pca = PCA(n_components=n)
    pca.fit(x_train)
    X_train_pca = pca.transform(x_train)
    X_test_pca = pca.transform(x_test)
    log_cols = ["Classifier", "Accuracy", "Score Train", "Log Loss", "Precision Score", 
                "Recall Score", "Time to fit", "Time to predict"]
    log = pd.DataFrame(columns=log_cols)
    #fitting(classifiers[0], X_train_pca, y_train)

    for clf in classifiers:
        print("=" * 30)
        print("Fit for classifier --> ", clf.__class__.__name__)

        start = time()

        fitting(clf, X_train_pca, y_train)
        #clf.fit(X_train_pca, y_train)
        print(" took %.2f seconds" % (time() - start))

        time_spent = (time() - start)
        score = clf.score(X_test_pca, y_test)
        

        print("=" * 30)
        print(clf.__class__.__name__)

        print("***** Results *****")
        time_init_pred = time()
        y_pred = clf.predict(X_test_pca)
        acc = accuracy_score(y_test, y_pred)
        
        time_predict = time() - time_init_pred
        print(" took %.2f seconds to predict on test dataset" % time_predict)
        ############# Precision #########################
        # The precision is the ratio tp / (tp + fp) where tp is the number of true positives and fp the number of false positives
        # The precision is intuitively the ability of the classifier not to label as positive a sample that is negative
        precision = precision_score(y_test, y_pred, average='weighted')

        ############# Recall Score ######################
        # The recall is the ratio tp / (tp + fn) where tp is the number of true positives and fn the number of false negatives
        # The recall is intuitively the ability of the classifier to find all the positive samples
        recall = recall_score(y_test, y_pred, average='weighted')

        print("Accuracy of the classifier: {:.3%}".format(acc))
        print("Precision score of the classifier: {:.3%}".format(precision))
        print("Score of the classifier: {:.3%}.".format(score))
        if hasattr(clf, "predict_proba"):
            train_predictions = clf.predict_proba(X_test_pca)
        else:
            conf_mat = confusion_matrix(y_true=y_test, y_pred=y_pred)
            train_predictions = clf.decision_function(X_test_pca)
            
        #plot confusion matrix if variable conf_matrix is set to True
        if conf_matrix:
            cnf_matrix = confusion_matrix(y_test, y_pred)
            np.set_printoptions(precision=2)

            # Plot non-normalized confusion matrix
            plt.figure()
            plot_confusion_matrix(cnf_matrix, title='matriz de confusion sin normalizacion')

            # Plot normalized confusion matrix
            plt.figure()
            plot_confusion_matrix(cnf_matrix, normalize=True,
                                  title='matriz de confusion con normalizacion')
            plt.show()
        
        ll = log_loss(y_test, train_predictions)
        print("Compute recall of the classifier: {:.3%}".format(recall))
        print("Log Loss: {}".format(ll))

        entry = pd.DataFrame([[clf.__class__.__name__, acc * 100, score * 100,
                               ll, precision * 100, recall * 100, time_spent, time_predict]], columns=log_cols)
        log = log.append(entry)
        print("=" * 30)
        return log


#########################################################################################
# Reference. Pages 128 - 135
# https://sebastianraschka.com/books.html#python-machine-learning-1st-edition
def self_pca(X):
    """
    
    """
    """Creates a covariance matrix and show a graph explaining variance ratio and variance cumulative around
    the different features"""
    # Bring different features onto the same scale : standardization (practical for many machine learning algorithms)
    sc = StandardScaler()
    X_train_std = sc.fit_transform(X)

    cov_mat = np.cov(X_train_std.T)
    eigen_vals, eigen_vecs = np.linalg.eig(cov_mat)
    tot = sum(eigen_vals)
    var_exp = [(i / tot) for i in sorted(eigen_vals, reverse=True)]
    cum_var_exp = np.cumsum(var_exp)

    # Plot explained ratio and cumulative variance 
    plt.step(range(1, 54), cum_var_exp, where='mid', label='cumulative explained variance')
    plt.bar(range(1, 54), var_exp, alpha=0.5, align='center', label='individual explained variance')
    plt.ylabel('Explained variance ratio')
    plt.xlabel('Principal Components')
    plt.legend(loc='best')
    plt.show()

    eigen_pairs = [(np.abs(eigen_vals[i]), eigen_vecs[:, i]) for i in range(len(eigen_vals))]
    eigen_pairs.sort()

    print(eigen_pairs)

    w = np.hstack((eigen_pairs[0][1][:, np.newaxis], 
                   eigen_pairs[1][1][:, np.newaxis],
                   eigen_pairs[2][1][:, np.newaxis]))
    X_train_std[0].dot(w)

    X_train_pca = X_train_std.dot(w)
    colors = ['r', 'b', 'g']
    markers = ['s', 'x', 'o']
    for l, c, m in zip(np.unique(y_train), colors, markers):
        plt.scatter(X_train_pca[y_train == l, 0], X_train_pca[y_train == l, 1], c=c, label=l, marker=m)
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.legend(loc='lower left')
    plt.show()



#########################################################################################
#########################################################################################


"""List of classifiers and their hyperparameters after some tunning using GridSearch. This list of classifiers
will be used to fit with X_train and y_train datasets and check which one is the best fitting for classifying"""

list_linear_model = [LogisticRegression(warm_start=True, fit_intercept=True, solver='newton-cg',
                                        multi_class='multinomial', random_state=1, max_iter=500, n_jobs=2),
                     Perceptron(alpha=0.001, fit_intercept= True, penalty='l1', random_state= 10,
                                shuffle= True, warm_start = True, n_jobs= 2)
                     ]

list_discriminant_analysis = [   LinearDiscriminantAnalysis(),
                                 QuadraticDiscriminantAnalysis()
]

list_support_vector = [   SVC(kernel='rbf',
                         C=10, decision_function_shape='ovr', verbose=True,
                         random_state=1, shrinking=False, tol=0.01, probability=True),
]

list_neighbors = [KNeighborsClassifier(n_neighbors=5, weights='uniform', 
                                       algorithm='auto', p=1)]
list_multilayerperc = [MLPClassifier(warm_start=False, shuffle=True, nesterovs_momentum=True, hidden_layer_sizes=7,
                       validation_fraction=0.333,
                       solver = 'adam',
                       learning_rate='constant', max_iter=162, batch_size=200, random_state=1,
                       momentum=0.11593,
                       tol=0.081977,
                       alpha=0.01, activation='relu', early_stopping=False)
]

list_naive_bayes = [   BernoulliNB(),
                       GaussianNB(),
                       MultinomialNB()
]

list_trees = [DecisionTreeClassifier(splitter="random", max_leaf_nodes=None, min_samples_leaf=1, min_samples_split=2,
                                     min_weight_fraction_leaf=0.0, criterion='entropy', random_state=5,
                                     max_features=None,
                                     max_depth=None),
              
              ]

list_ensemble = [AdaBoostClassifier(n_estimators = 50, random_state=None, learning_rate=1, algorithm='SAMME'),
       AdaBoostClassifier(base_estimator=RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
                                                                max_depth=None, max_features='auto',
                                                                min_impurity_decrease=0.0, min_impurity_split=None,
                                                               max_leaf_nodes=None,
                                                                min_weight_fraction_leaf=0.0,
                                                                min_samples_leaf=1, min_samples_split=2,
                                                                n_estimators=10, oob_score=False, random_state=None,
                                                                warm_start=False),
                        random_state=1, learning_rate=0.5, algorithm='SAMME'),
       AdaBoostClassifier(base_estimator=ExtraTreesClassifier(bootstrap=True, class_weight=None, criterion='gini',
                                                                max_depth=None, max_features='auto',
                                                                max_leaf_nodes=None,
                                                                min_impurity_decrease=0.0, min_impurity_split=None,
                                                                min_samples_leaf=1, min_samples_split=2,
                                                                min_weight_fraction_leaf=0.0,
                                                                n_estimators=10, oob_score=False, random_state=None,
                                                                warm_start=False),
                         random_state=1, learning_rate=0.5, algorithm='SAMME'),
      RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
                           max_depth=None, max_features='auto', max_leaf_nodes=None,
                                 min_impurity_decrease=0.0, min_impurity_split=None,
                           min_samples_leaf=1, min_samples_split=2,
                          min_weight_fraction_leaf=0.0,
                           n_estimators=10, oob_score=False, random_state=None,
                           warm_start=False),
      ExtraTreesClassifier(bootstrap=False, criterion='entropy', max_depth=None, max_leaf_nodes=None,
                                    min_samples_leaf=1,
                                    min_samples_split=2, min_weight_fraction_leaf=0.0, random_state=5, warm_start=False)
]

#Uncomment to iterate over a technique
list_total_classifiers = [  # list_linear_model,
                            #list_discriminant_analysis,
                            # list_support_vector,
                            # list_neighbors,
                            # list_multilayerperc,
                            # list_naive_bayes,
                            # list_trees,
                            # list_ensemble
]


if __name__ == "__main__":

    init = time()

    # Import datasets for training and test
    start = time()

    print("Reading dataset")
    
    train = pd.read_csv('trees.csv')
    
    #Uncomment next 2 lines to read metric values from CSV file and show them in plots
    
    #values = pd.read_csv('neighbors_ALL.csv')
    #show_graphs(values)

    print("Read took %.2f seconds for %d samples."
          % (time() - start, len(train)))
    print("+" * 30)


    start = time()
    

    X, y = preprocess(train)
    X1 = standardize(X)
    #X2 = minmaxscalizer(X)

    sss = StratifiedShuffleSplit(10, test_size=0.3, random_state=0)
    for train_index, test_index in sss.split(X, y):
        X_train, X_test = X.values[train_index], X.values[test_index]
        y_train, y_test = y[train_index], y[test_index]
        
    #iterate over each technique and each classifier
    for n in range (0, len(list_total_classifiers)):
        log = compare_classifiers(list_total_classifiers[n], X_train, y_train, X_test, y_test,conf_matrix=False)
        #Uncomment next line if you want to save the results stored in log variable to a csv file
        log.to_csv('name.csv', mode='a', header=False, sep=',')
    
    
    #Cross validation for X values (first 10 atributes) after standardization
    for train_index, test_index in sss.split(X1, y):
        X_train, X_test = X1[train_index], X1[test_index]
        y_train, y_test = y[train_index], y[test_index]
    
    for n in range (0, len(list_total_classifiers)):
        log = compare_classifiers(list_total_classifiers[n], X_train, y_train, X_test, y_test,conf_matrix=False)
        #Uncomment next line if you want to save the results stored in log variable to a csv file
        log.to_csv('name.csv', mode='a', header=False, sep=',')
   
    #####PCA####
    #Uncomment to do training with PCA 
    for n in range (0, len(list_total_classifiers)):
        log_pca5=pca(list_total_classifiers[n], X_train, y_train, X_test, y_test, 5)
        
        #Uncomment next line if you want to save the results stored in log variable to a csv file
        #log_pca5.to_csv('name.csv', mode='a', header=False, sep=',')
        
        log_pca10=pca(list_total_classifiers[n], X_train, y_train, X_test, y_test, 10)
        #log_pca10.to_csv('name.csv', mode='a', header=False, sep=',')
    

    #Uncomment to show a plot of individual and cumulative explanied variance   
    #self_pca(X_train)
    
    # Uncomment  to do a GridSearch. Be aware to use the proper name for classifier
    # and dict associated to that classifier.
    #grid_search(KNeighborsClassifier(), KneighborsGrid)
    end = time()
    print("script duration: %r seconds" % (end - start))
