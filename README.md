# Goal
A python script for analysis of some machine learning algorithms for classification using scikit-learn library. 
This script has been developed for a final project in Computer Engineer Master in Complutense University of Madrid.
This script has been setup for a individual classification research for a specific dataset. 
The dataset could be downloaded from [UCI Cover Type](https://archive.ics.uci.edu/ml/datasets/covertype)

# Abstract
We could say that we live today surrounded by the generation of large amount of data. The majority of that data is too much huge to allow humans to be able to get the proper tools which to store, process and extract the information that, treated and made available to us, we can extract the right benefit for a specific objective.

For the treatment of this information we can focus on the branch of Artificial Intelligence called machine learning which allows machines to learn and make decisions automatically with future data provided.

We could say that learning is done thanks to the detection of patterns within a set of data where the algorithm is able to predict what kind of situations could occur. What we intend to do is to use the historical data which, properly sorted and treated in blocks, it generates a database that can be used to predict future behaviors, or, for example, we can classify a new data set not previously observed. An example of this could be the facial recognition technique.

For this project, we will use the library for Python programming language scikit-learn, which is used for machine learning. It has a large number of algorithms for classification where we will find different behaviour depending on the quantity and distribution of the data provided. From these algorithms we can extract some metrics, such as the use of resources from the machine, which can be used to carry out a comparison and analysis.

A comparison and analysis of the behavior of each algorithm will be made in the prediction of 7 forest cover types with the use of cartographic variables through values taken in 4 different areas of the Roosevelt National Park in the north of the state of Colorado.


